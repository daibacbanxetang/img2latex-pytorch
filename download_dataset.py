import os
import requests
import tarfile
from os.path import join, exists
from config import *
from tqdm import tqdm


def download(link):
    file_name = link.split('/')[-1]
    file_path = join(data_path, file_name)

    if os.path.exists(file_path):
        print(f"{file_name} was existed")
        return

    print(f"Downloading {file_name} file")
    res = requests.get(link, stream=True)
    with open(file_path, 'wb') as f:
        for chunk in tqdm(res.iter_content(chunk_size=512)):
            if chunk:
                f.write(chunk)


def extract_data(tar_path, extract_path):
    if not exists(tar_path):
        raise Exception("Formula images tar didn't existed. "
                        "Please downloading dataset before extract file!")

    if exists(join(extract_path, 'formula_images_processed')):
        print('Data was extracted.')
    else:
        tar = tarfile.open(tar_path)
        tar.extractall(extract_path)


def main():
    with open(data_links, 'r') as f:
        lines = f.read().splitlines()

    for link in lines:
        download(link)
    extract_data(join(data_path, 'formula_images_processed.tar.gz'),
                 data_path)


if __name__ == '__main__':
    main()